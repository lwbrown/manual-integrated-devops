apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
    kubernetes.io/tls-acme: "true"
  generation: 5
  name: ${CI_ENVIRONMENT_SLUG}-nginx-ingress
spec:
  rules:
  - host: ${CI_ENVIRONMENT_URL}
    http:
      paths:
      - backend:
          serviceName: ${CI_ENVIRONMENT_SLUG}-nginx
          servicePort: 80
        path: /
  tls:
  - hosts:
    - ${CI_ENVIRONMENT_URL}
    secretName: ${CI_ENVIRONMENT_SLUG}-ingress-tls
