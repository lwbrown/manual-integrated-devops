apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  labels:
    app: ${CI_ENVIRONMENT_SLUG}-nginx
  name: ${CI_ENVIRONMENT_SLUG}-nginx
  annotations:
    app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
    app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
spec:
  replicas: ${REPLICAS}
  selector:
    matchLabels:
      app: ${CI_ENVIRONMENT_SLUG}-nginx
  template:
    metadata:
      labels:
        app: ${CI_ENVIRONMENT_SLUG}-nginx
      annotations:
        app.gitlab.com/app: ${CI_PROJECT_PATH_SLUG}
        app.gitlab.com/env: ${CI_ENVIRONMENT_SLUG}
    spec:
      containers:
      - image: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}
        imagePullPolicy: IfNotPresent
        name: ${CI_ENVIRONMENT_SLUG}-nginx
        livenessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 3
          periodSeconds: 3
        readinessProbe:
          httpGet:
            path: /
            port: 80
          initialDelaySeconds: 10
          periodSeconds: 5
